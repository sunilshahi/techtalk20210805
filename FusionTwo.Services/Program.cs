﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using System.Threading.Tasks;

namespace FusionTwo.Services
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var host = Host.CreateDefaultBuilder(args)
                .ConfigureServices((context, services) =>
                {
                    //services.AddTransient<IMyService, MyService>();
                })
                .UseSerilog((context, logger) =>
                {
                    logger
                        .Enrich.FromLogContext()
                        .ReadFrom.Configuration(context.Configuration);
                        //.MinimumLevel.Information()
                        //.WriteTo.Console(
                        //    outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} <s:{SourceContext}>{NewLine}{Exception}",
                        //    theme: Serilog.Sinks.SystemConsole.Themes.AnsiConsoleTheme.Code
                        //)
                        //.WriteTo.MicrosoftTeams(
                        //    webHookUri: "https://fusionstak2.webhook.office.com/webhookb2/aba4a9dd-9aa3-468f-bcf7-7dcbb1266f18@a0539cf6-a3a3-481e-a675-1ae8acbb3089/IncomingWebhook/01f4402621e149dba2e46bf170a3abfb/3c8ae265-0ec5-46fd-bdd0-25a3cd049a79",
                        //    title: "Sunil Did something"
                        //);
                        //.WriteTo.File(
                        //    path: ".\\log.txt",
                        //    rollingInterval: RollingInterval.Day,
                        //    retainedFileCountLimit: 2
                        //)
                        //.WriteTo.DatadogLogs(
                        //    apiKey: "*****PUT YOUR DATADOG API KEY HERE*****",
                        //    tags: new[] { "test1", "test2" },
                        //    source: "testapp",
                        //    service: "test.service"
                        //);
                })
                .Build();

            var service = ActivatorUtilities.GetServiceOrCreateInstance<MyService>(host.Services);
            service.DoStuff();

            await Task.Delay(TimeSpan.FromSeconds(5));
        }
    }

    public class MyService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<MyService> _logger;
        public MyService(
            IConfiguration configuration, ILogger<MyService> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }
        public void DoStuff()
        {
            _logger.LogError("User created {user} at {time}.", new
            {
                UserName = "sunilshahi",
                Email = "sunil@fusionstak.com"
            }, DateTime.UtcNow);
            //_logger.LogInformation(_configuration["Greeting"]);
        }
    }
}
